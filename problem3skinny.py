import math
initial_factors=[]
after_factors=[]
after_factors_2=[]
last_factors=[]
bad_count=[]
x=int(raw_input("> "))
i=1
while (2+i)<=math.sqrt(x):
	if x%(2+i)==0:
		initial_factors.append(2+i)
		i+=2
	else:
		i+=2
for factor in initial_factors:
	for divisor in range(1,9):
		if factor % divisor != 0:
			after_factors.append(factor)
for d,factor in enumerate(after_factors):
	while d<len(after_factors) and after_factors[d] not in after_factors_2:
		if after_factors[d]==after_factors[d+1]:
			after_factors_2.append(after_factors[d])
			d+=1
		else:
			d+=1
for factor in after_factors_2:
	bad_count=0
	for c in range(0,len(after_factors_2)):
		if factor%after_factors_2[c]==0 and factor/after_factors_2[c]!=1:
			bad_count+=1
	if bad_count==0:
		last_factors.append(factor)
print "The factors of %d are %s" % (x,str(last_factors))