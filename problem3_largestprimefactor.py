import math
initial_factors=[]
after_factors=[]
after_factors_2=[]
last_factors=[]
bad_count=[]
x=int(raw_input("Enter an integer with no spaces or commas to find its factorization.\nOdd numbers are prime factors, so will not work generally.\n> "))
i=1
if x%2==0:
	i=x
	j=i
	twos=0
	factor_list=[]

	if i % 2 ==0:
		while i%2==0:
			i=i/2
			twos+=1
			#print i
		twostring="2^%d" % (twos)
		factor_list.append(twostring)
		if i!=1:
			factor_list.append(i)
	print "The factors of %d are %s" % (j,str(factor_list))
if x%2!=0:
	while (2+i)<=math.sqrt(x):
		if x%(2+i)==0:
			initial_factors.append(2+i)
			i+=2
		else:
			i+=2
	for factor in initial_factors:
		for divisor in range(1,9):
			if factor % divisor != 0:
				after_factors.append(factor)
	#print after_factors
	for d,factor in enumerate(after_factors):
		while d<len(after_factors) and after_factors[d] not in after_factors_2:
			if after_factors[d]==after_factors[d+1]:
				after_factors_2.append(after_factors[d])
				d+=1
			else:
				d+=1
	#print after_factors_2
	# for factor in after_factors_2:
		# for factor_divisor in after_factors_2:
			# if factor % factor_divisor !=0:
				# print factor, factor_divisor
				# last_factors.append(factor)
	# print last_factors
	# for d,factor in enumerate(after_factors_2):
		# c=d
		# while d<len(after_factors_2) and c>=0:
			# j=d+1
			# bad_count=0
			# print after_factors_2[j]
			# print after_factors_2[c]
			# if after_factors_2[j]%after_factors_2[c]==0:
				# bad_count+=1
				# c-=1
				# print bad_count
			# else:
				#if bad_count==0:
					#last_factors.append(j)
				# c-=1
				# d+=1
			# if bad_count==0:
				# last_factors.append(j)
	for factor in after_factors_2:
		bad_count=0
		for c in range(0,len(after_factors_2)):
			if factor%after_factors_2[c]==0 and factor/after_factors_2[c]!=1:
				bad_count+=1
				#print bad_count
		if bad_count==0:
			last_factors.append(factor)
	print "The factors of %d are %s" % (x,str(last_factors))