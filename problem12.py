__author__ = 'joe'

import time

start_time = time.clock()


def get_triangle_number(position):
    return sum([i for i in xrange(1, position + 1)])


def product(*args):
    for arg in args:
        if not arg:
            return
        if isinstance(arg, list):
            for i in range(0, len(arg)):
                if i == 0:
                    l = arg[i]
                else:
                    l *= arg[i]
            return l
        else:
            for i in range(0, len(args)):
                if i == 0:
                    l = args[i]
                else:
                    l *= args[i]
            return l


def primes_to_(number):
    array = [True] * number
    for x in xrange(2, int(len(array) ** .5) + 1):
        if array[x] is True:
            for i in xrange(x + x, len(array), x):
                array[i] = False
    return [i for i in xrange(2, len(array)) if array[i]]


def tau(primeexps):
    divisors = 1
    for primeexp in primeexps.values():
        divisors *= (primeexp + 1)
    return divisors


def get_prime_denoms(number, primes):
    if number == 0:
        return {0: 0}
    denoms = {}
    for prime in primes:
        if number % prime == 0:
            denoms[prime] = 1
            exp = 2
            while number % prime ** exp == 0:
                exp += 1
            denoms[prime] = exp - 1
        if product([denom ** exp for denom, exp in denoms.items()]) == number:
            return denoms

    return {0: 0}


primes = primes_to_(500000)
num = 0
limit=500

p = tau(get_prime_denoms(get_triangle_number(num), primes))

while p < limit:
    num += 1
    p = tau(get_prime_denoms(get_triangle_number(num), primes))
    #print p

print "First number over %d is %d"%(limit,get_triangle_number(num))

print "finished in:", time.clock() - start_time, 'seconds'
